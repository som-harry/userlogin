using System;
using MongoDB.Driver;
using UserLogin.Models;

namespace UserLogin.Database
{
    public class Mongo
    {
        public static IMongoDatabase connect()
        {
            var dbClient = new MongoClient("mongodb://127.0.0.1:27017");

            if (dbClient.ListDatabases() == null)
            {
                Console.WriteLine("Check and connect Back");
            }
            else
            {
                Console.WriteLine(dbClient.ListDatabases());
                Console.WriteLine("Connected successfully");
            }

            return dbClient.GetDatabase("UserLogin");
        }

        public static IMongoCollection<UserModel> UserModel()
        {
            var database = connect();
            return database.GetCollection<UserModel>("Users");
        }
    }
}