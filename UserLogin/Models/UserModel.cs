using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;

namespace UserLogin.Models
{
    public class UserModel
    {
        public ObjectId Id;
        
        [Required]
        public string  Name { get; set; }
        
        [Required]
        public string Username { get; set; }
        
        [Required]
        public string Password { get; set; }

        
        public UserModel()
        {
            Id = new ObjectId();
        }

        public override string ToString()
        {
            return $"Username is {Username}";
        }
    }
}