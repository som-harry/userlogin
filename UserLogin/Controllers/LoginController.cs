using System;
using System.Web.Mvc;
using DevOne.Security.Cryptography.BCrypt;
using MongoDB.Driver;
using UserLogin.Database;
using UserLogin.Models;

namespace UserLogin.Controllers
{
    public class LoginController : Controller
    {

        //GEt
        [HttpGet]
        public ActionResult Login()
        {
            return View("Login");
        }
        //Post
        [HttpPost]
        public ActionResult Login(UserModel userModel, string username, string password)
        {
            //return View("Login");
            if (userModel.Username == username && userModel.Password == password)
            {
                return View("Dashboard", userModel);
            }
            else
            {
                ViewBag.Error = "Wrong Credential";
                return View("Error");
            }
        }

        //get
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        //Register
        [HttpPost]
        public ActionResult Register(UserModel userModel, string ConfirmPassword)
        {
            //check whether the current user is in the database
            if (userModel.Username == null)
            {
                ViewBag.Error = "User name cannot be empty";
                return View("Error");
            }
            var UserModel = Mongo.UserModel();
            var filter = Builders<UserModel>.Filter.Eq("Username", userModel.Username);
            try
            {
                var databaseUser = UserModel.FindSync<UserModel>(filter).FirstOrDefault();
                if (databaseUser == null)
                {
                    if (userModel.Name == null && userModel.Password == null && userModel.Username == null)
                    {
                        ViewBag.Error = "All field should be filled";
                        return View("Error");
                    }

                    if (userModel.Password != ConfirmPassword)
                    {
                        ViewBag.password = false;
                        ViewBag.message = "Passport mixedMatch";
                        return View("Error");
                    }

                    var salt = BCryptHelper.GenerateSalt();
                    userModel.Password = BCryptHelper.HashPassword(userModel.Password, salt);
                    UserModel.InsertOneAsync(userModel);

                }
                else
                {
                    ViewBag.Error = "Username already exits";
                    return View("Error");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return View("Login");
            
        }

        public ActionResult Logout()
        {
            return View("Login");
        }
    }

}
